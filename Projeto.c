#include <stdio.h>
#include <stdlib.h>

void tabuleiro(char casa[3][3]){ //funcao para rodar o tabluleiro
	system("cls");
	printf("\t  Colunas");
	printf("\n");
	printf("\t 1   2   3");
	printf("\n\n");
	printf("\t %c | %c | %c   1\n", casa[0][0],casa[0][1],casa[0][2]);
	printf("\t-----------\n");
	printf("\t %c | %c | %c   2 linhas\n", casa[1][0],casa[1][1],casa[1][2]);
	printf("\t-----------\n");
	printf("\t %c | %c | %c   3\n", casa[2][0],casa[2][1],casa[2][2]);
	printf("\n");
}

int testadorX(char casa[3][3]){ //verifica as condicoes de vitoria do X e retorna o valor 11
	int cont;
	if(casa[0][0] == 'X' && casa[0][1] == 'X' && casa[0][2] == 'X'){
	cont = 11;
	}
	if(casa[1][0] == 'X' && casa[1][1] == 'X' && casa[1][2] == 'X'){
	cont = 11;
	}
	if(casa[2][0] == 'X' && casa[2][1] == 'X' && casa[2][2] == 'X'){
	cont = 11;
	}
	if(casa[0][0] == 'X' && casa[1][0] == 'X' && casa[2][0] == 'X'){
	cont = 11;
	}
	if(casa[0][1] == 'X' && casa[1][1] == 'X' && casa[2][1] == 'X'){
	cont = 11;
	}
	if(casa[0][2] == 'X' && casa[1][2] == 'X' && casa[2][2] == 'X'){
	cont = 11;
	}
	if(casa[0][0] == 'X' && casa[1][1] == 'X' && casa[2][2] == 'X'){
	cont = 11;
	}
	if(casa[0][2] == 'X' && casa[1][1] == 'X' && casa[2][0] == 'X'){
	cont = 11;
	}
	return cont;
}

int testadorO(char casa[3][3]){ //verifica as condicoes de vitoria do O e retorna o valor 12
	int cont;
	if(casa[0][0] == 'O' && casa[0][1] == 'O' && casa[0][2] == 'O'){
	cont = 12;
	}
	if(casa[1][0] == 'O' && casa[1][1] == 'O' && casa[1][2] == 'O'){
	cont = 12;
	}
	if(casa[2][0] == 'O' && casa[2][1] == 'O' && casa[2][2] == 'O'){
	cont = 12;
	}
	if(casa[0][0] == 'O' && casa[1][0] == 'O' && casa[2][0] == 'O'){
	cont = 12;
	}
	if(casa[0][1] == 'O' && casa[1][1] == 'O' && casa[2][1] == 'O'){
	cont = 12;
	}
	if(casa[0][2] == 'O' && casa[1][2] == 'O' && casa[2][2] == 'O'){
	cont = 12;
	}
	if(casa[0][0] == 'O' && casa[1][1] == 'O' && casa[2][2] == 'O'){
	cont = 12;
	}
	if(casa[0][2] == 'O' && casa[1][1] == 'O' && casa[2][0] == 'O'){
	cont = 12;
	}
	return cont;
}



int main(){
	int cont_jog, res, lin, col, vez, i, j = 0;		//iniciamos as variaveis
	int iniciar;
	
	char casa[3][3] = { {'1','2','3'},
						{'4','5','6'},
						{'7','8','9'}, };	
							
	do{
		cont_jog = 1; 						//iniciamos com a primeira jogada e depois limpamos o tabuleiro
		for(i=0;i<=2;i++){					//(definindo todos o valores para vazio)
			for(j=0;j<=2;j++){
				casa[i][j] = ' ';
			}
		}
		do{
			tabuleiro(casa);						//imprime o tabuleiro
			if(vez%2==0){							//mostra de quem é a vez
				printf("     Jogador X\n");
				printf("\n");
			}else{
				printf("     Jogador O\n");
				printf("\n");
			}
				printf("  Digite a linha da jogada: ");
				scanf("%d", &lin);
				printf("  Digite a coluna da jogada: ");
				scanf("%d", &col);
				if (lin < 1 || lin > 3 || col < 1 || col > 3){		//se a jogador digitar a linha ou coluna menor q 1 e maior q 3
					lin = 0;										//vai pedir novamente para o jogador digitar as cordenadas
					col = 0;
				}
				else if(casa[lin-1][col-1] != ' '){
					lin = 0;
					col = 0;
				}
				else{
					if (vez %2 == 0){								//verifica quem fez a jogada com base na vez
						casa[lin-1][col-1] =  'X';					//se a vez for par atribui X a linha-1 e a coluna-1
					}
					else{
					casa[lin-1][col-1] =  'O';						//se for impar atribui O 
					}
					vez++;											// passa a vez
					cont_jog++;										// adiciona uma jogada
				}
				//condições do jogador X ganhar
				cont_jog = testadorX(casa);							//atribui a variavel cont_jogadas o retorno do testador
				//Condições do jogador O ganhar
				cont_jog = testadorO(casa);
				
			}while(cont_jog <=9);
			tabuleiro(casa);										//imprime o tabuleiro
			if (cont_jog == 10){
				printf("Deu velha!!\n");							//se ouveremm 10 jogadas empatou
			}
			if (cont_jog == 11){
				printf("X venceu!!\n");								//se retornar 11 do testador x vence
			}
			if (cont_jog == 12){
				printf("O venceu!!\n");								//se retornar 12 do testador o vence
			}
			printf("\n");
			printf("Os usuarios desejam jogar novamente?\nDigite [S ou N]: ");		//encerra ou reicinica o codigo
			scanf("%s", &res);
		}while(res == 's');
	
	return 0; 
}
